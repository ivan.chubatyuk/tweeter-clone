# Twitter clone

### Installing
- First of all you need to clone this project from GitLab.\
Just run the following command:
```
git clone https://gitlab.skillbox.ru/ivan_chubatiuk/python_advanced_diploma.git
```
- Go inside of installed directory and install app with database
```
cd python_advanced_diploma
docker-compose up -d
```

##### Now your app is started successfully!

##### but...
If you've got any problems - it can be caused by your docker-container started/
on another IP address. OK! So now you need to run this command:
```
docker ps
```
You will see some of your docker containers.
You need one which is called 'postgres'
- Copy postgres container ID
- Run following command:
```
docker inspect <container_id>
```
You gonna see a lot of information, but you need IPv6 address
- Copy that IP address
- Go inside of your docker container running the following command:
```
docker exec -it <application_container_id> /bin/bash
nano app/app/database.py
```
You are pretty done!

Here you will see some code for your database...\
Look for 'DATABASE_URL'\
In the end you will see database url
- Change it for the url that you have copied earlier
- Now press Ctrl+O
- And Ctrl+X

#### Last step!!!
- Ctrl+D
- Aaaand:
```
docker restart <application_container_id>
```
#### Done!!!

### How to use this application

Once your app was successfully started - you need to open your browser
and go to the [Tweeter main page](http://127.0.0.1:80/login).\
Here you can see a page where you can create a new tweet.

- Press 'tweet' to share your tweet with others
- On the right top angle you will see white window, where you can switch user.\
Available users api-keys: 
    - test (Name: new)
    - test2 (Name: new2)

- You can see your profile by clicking on the button 'Profile'