import hashlib
from fastapi import FastAPI, HTTPException, Header, UploadFile, File
from fastapi.staticfiles import StaticFiles
from fastapi.responses import HTMLResponse
from sqlalchemy.future import select
from sqlalchemy.orm import joinedload
from sqlalchemy import desc, func
from .database import session, engine
import app.models as models
import app.schemas as schemas
import os


app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.drop_all)
        await conn.run_sync(models.Base.metadata.create_all)
    api_key1 = 'test'.encode()
    api_key1 = hashlib.sha256(api_key1).hexdigest()
    api_key2 = 'test2'.encode()
    api_key2 = hashlib.sha256(api_key2).hexdigest()
    user1 = models.User(name="new", api_key=api_key1)
    user2 = models.User(name="new2", api_key=api_key2)
    session.add_all([user1, user2])
    await session.commit()


@app.on_event("shutdown")
async def shutdown():
    await session.close()
    print("done")


@app.exception_handler(HTTPException)
async def http_exception_handler(request, exc):
    return {
        "result": False,
        "error_type": "HTTPException",
        "error_message": str(exc.detail),
    }


@app.exception_handler(Exception)
async def generic_exception_handler(request, exc):
    return {
        "result": False,
        "error_type": type(exc).__name__,
        "error_message": str(exc),
    }


@app.get("/login")
async def get_login_page():
    with open("../app/static/index.html", "r") as f:
        return HTMLResponse(content=f.read(), status_code=200)


@app.post("/api/tweets", response_model=schemas.TweetCreateResponse)
async def create_tweet(
        tweet_data: schemas.TweetCreateRequest,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    tweet_id = await models.save_tweet_to_database(
        tweet_data.tweet_data, tweet_data.tweet_media_ids, api_key
    )
    response = schemas.TweetCreateResponse(result=True, tweet_id=tweet_id)
    return response


@app.post("/api/medias", response_model=schemas.MediaUploadResponse)
async def download_from_tweet(
        file: UploadFile = File(...),
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    media_id = await models.save_file(file, api_key)
    response = schemas.MediaUploadResponse(result=True, media_id=media_id)

    return response


@app.delete("/api/tweets/{idx}", response_model=schemas.Result)
async def delete_tweet(
        idx: int,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    delete = await models.delete_tweet(idx, user)
    if delete:
        return schemas.Result(result=True)
    return False


@app.post("/api/tweets/{idx}/likes", response_model=schemas.Result)
async def like_tweet(
        idx: int,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")

    existing_like = await session.execute(
        select(models.Like).filter(
            models.Like.user_id == user.id, models.Like.tweet_id == idx
        )
    )
    existing_like = existing_like.scalars().first()
    for like in user.likes:
        print(like.tweet_id)

    if existing_like:
        delete = await models.delete_like(idx, user)
        if delete:
            return {"result": True}

    like = models.Like(user_id=user.id, tweet_id=idx)
    session.add(like)
    await session.commit()
    session.expire_all()

    return schemas.Result(result=True)


@app.get("/api/tweets/{idx}/likes", response_model=schemas.Result)
async def unlike_tweet(
        idx: int,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")

    like = models.delete_like(idx, user)
    if like:
        return schemas.Result(result=True)


@app.post("/api/users/{id}/follow", response_model=schemas.Result)
async def follow_user(
        id: int,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    user_to_follow = await session.execute(
        select(models.User).where(models.User.id == id)
    )
    user_to_follow = user_to_follow.scalars().first()
    following = models.Following(
        name=user_to_follow.name, user_id=user_to_follow.id, following=user
    )
    follower = models.Followers(
        name=user.name, follower_id=user.id, followers=user_to_follow
    )
    session.add_all([following, follower])
    await session.commit()
    session.expire_all()

    return schemas.Result(result=True)


@app.delete("/api/users/{id}/follow", response_model=schemas.Result)
async def unfollow_user(
        id: int,
        api_key: str = Header(..., title="Api-key header of the user for authentication"),
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    user_to_follow = await session.execute(
        select(models.User).where(models.User.id == id)
    )
    user_to_follow = user_to_follow.scalars().first()
    exists = await session.execute(
        select(models.Followers).filter(
            models.Followers.follower_id == user.id,
            models.Followers.user_id == user_to_follow.id,
            )
    )
    exists = exists.scalars().first()
    if exists:
        delete = await models.unfollow_user(user, user_to_follow, exists)
        if delete:
            return {"result": True}
    following = models.Following(
        name=user_to_follow.name, user_id=user_to_follow.id, following=user
    )
    follower = models.Followers(
        name=user.name, follower_id=user.id, followers=user_to_follow
    )
    session.add_all([following, follower])
    await session.commit()
    session.expire_all()
    return schemas.Result(result=True)


@app.get("/api/tweets", response_model=schemas.AllTweets)
async def get_tweets(
        api_key: str = Header(..., title="Api-key header of the user for authentication")
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    subquery = (
        select(models.Like.tweet_id, func.count(models.Like.id).label("like_count"))
        .group_by(models.Like.tweet_id)
        .subquery()
    )

    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    if not user:
        raise HTTPException(status_code=401, detail="401 Unauthorized")
    tweets = await session.execute(
        select(models.Tweets)
        .options(
            joinedload(models.Tweets.user),
            joinedload(models.Tweets.media),
            joinedload(models.Tweets.likes_all),
        )
        .join(subquery, models.Tweets.id == subquery.c.tweet_id, isouter=True)
        .order_by(desc(subquery.c.like_count).nullslast())
    )
    tweets = tweets.unique().scalars().all()

    tweets_result = [
        schemas.Tweet(
            id=tweet.id,
            content=tweet.tweet_data,
            attachments=[os.path.normpath(media.media_path) for media in tweet.media],
            author=schemas.Followers(id=tweet.user_id, name=tweet.tweets.name),
            likes=[
                schemas.Like(id=like.user_id, name=like.likes.name)
                for like in tweet.likes_all
            ],
        )
        for tweet in tweets
    ]

    return schemas.AllTweets(result=True, tweets=tweets_result)


@app.get("/api/users/me", response_model=schemas.UserByID)
async def get_user_me(
        api_key: str = Header(..., title="Api-key header of the user for authentication")
):
    api_key = api_key.encode()
    api_key = hashlib.sha256(api_key).hexdigest()
    user = await session.execute(
        select(models.User).where(models.User.api_key == api_key)
    )
    user = user.scalars().first()
    followers = [
        schemas.Followers(id=follower.id, name=follower.name)
        for follower in user.followers
    ]
    followings = [
        schemas.Following(id=following.id, name=following.name)
        for following in user.following
    ]
    user = schemas.User(
        id=user.id, name=user.name, followers=followers, following=followings
    )
    result = schemas.UserByID(result=True, user=user)
    return result


@app.get("/api/users/{idx}", response_model=schemas.UserByID)
async def get_user_by_id(idx: int, api_key: str = Header(...)):
    user = await session.execute(select(models.User).where(models.User.id == idx))
    user = user.scalars().first()
    followers = [
        schemas.Followers(id=follower.id, name=follower.name)
        for follower in user.followers
    ]
    followings = [
        schemas.Following(id=following.id, name=following.name)
        for following in user.following
    ]
    user = schemas.User(
        id=user.id, name=user.name, followers=followers, following=followings
    )
    result = schemas.UserByID(result=True, user=user)
    return result


app.mount("/app", StaticFiles(directory="../app"), name="uploads")
app.mount("/", StaticFiles(directory="../app/static"), name="static")

