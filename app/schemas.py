from pydantic import BaseModel, Field
from typing import List


class Followers(BaseModel):
    id: int = Field(...)
    name: str = Field(...)


class Following(Followers):
    ...


class User(BaseModel):
    id: int = Field(...)
    name: str = Field(...)
    followers: List[Followers] = Field(...)
    following: List[Following] = Field(...)


class UserByID(BaseModel):
    result: bool = Field(...)
    user: User


class TweetCreateRequest(BaseModel):
    tweet_data: str = Field(...)
    tweet_media_ids: List[int] = Field(...)


class TweetCreateResponse(BaseModel):
    result: bool = Field(...)
    tweet_id: int = Field(...)


class MediaUploadResponse(BaseModel):
    result: bool = Field(...)
    media_id: int = Field(...)


class Result(BaseModel):
    result: bool = Field(...)


class Like(BaseModel):
    id: int = Field(...)
    name: str = Field(...)


class Tweet(BaseModel):
    id: int = Field(...)
    content: str = Field(...)
    attachments: List[str] = Field(...)
    author: Followers = Field(...)
    likes: List[Like] = Field(...)


class AllTweets(BaseModel):
    result: bool = Field(...)
    tweets: List[Tweet] = Field(...)

