import uuid
from typing import List

from fastapi import UploadFile
from sqlalchemy import Column, Integer, String, ForeignKey, BigInteger, and_, update
from sqlalchemy.orm import relationship, joinedload
from .database import Base, session
from sqlalchemy.future import select
from pathlib import Path
import os


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    api_key = Column(String, nullable=False, unique=True)
    followers = relationship("Followers", backref="followers", lazy="selectin")
    following = relationship("Following", backref="following", lazy="selectin")
    tweets = relationship("Tweets", backref="tweets", lazy="joined")
    tweets_media = relationship("TweetsMedia", backref="tweets_media", lazy="selectin")
    likes = relationship("Like", backref="likes", lazy="selectin")

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Followers(Base):
    __tablename__ = "followers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    follower_id = Column(Integer)
    user_id = Column(ForeignKey("user.id"))

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Following(Base):
    __tablename__ = "following"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    user_id = Column(Integer)
    user_following_id = Column(ForeignKey("user.id"))

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Like(Base):
    __tablename__ = "like"

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey("user.id"))
    tweet_id = Column(ForeignKey("tweets.id"))


class TweetsMedia(Base):
    __tablename__ = "tweets_media"

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey("user.id"))
    tweet_id = Column(ForeignKey("tweets.id"))
    media_path = Column(String)
    media_id = Column(BigInteger)


class Tweets(Base):
    __tablename__ = "tweets"

    id = Column(Integer, primary_key=True)
    user_id = Column(ForeignKey("user.id"))
    tweet_data = Column(String)
    likes_all = relationship(
        "Like", backref="like", lazy="joined", cascade="all, delete"
    )
    media = relationship("TweetsMedia", backref="media_tweets", lazy="joined")
    user = relationship("User", backref="user", lazy="joined")

    def to_json(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


async def save_tweet_to_database(
        tweet_data: str, tweet_media_ids: List[int], api_key: str
):
    user = await session.execute(select(User).where(User.api_key == api_key))
    user = user.scalars().first()

    tweet = Tweets(user_id=user.id, tweet_data=tweet_data)
    session.add(tweet)
    await session.commit()

    tweet = await session.execute(
        select(Tweets).filter(
            Tweets.user_id == user.id,
            Tweets.tweet_data == tweet_data,
            )
    )
    tweet = tweet.scalars().first()

    update_query = (
        update(TweetsMedia)
        .where(
            and_(TweetsMedia.user_id == user.id, TweetsMedia.id.in_(tweet_media_ids))
        )
        .values(tweet_id=tweet.id)
    )
    await session.execute(update_query)
    await session.commit()

    return tweet.id


async def save_file(file: UploadFile, api_key):
    file_extension = Path(file.filename).suffix
    file_name = f"{uuid.uuid4()}{file_extension}"
    os.makedirs("./app/app/uploads", exist_ok=True)
    save_path = os.path.join("../app/uploads", file_name)

    with open(save_path, "wb") as f:
        f.write(file.file.read())

    media_id = hash(save_path)
    user = await session.execute(select(User).where(User.api_key == api_key))
    user = user.scalars().first()
    tweets_media = TweetsMedia(user_id=user.id, media_path=save_path, media_id=media_id)
    session.add(tweets_media)
    await session.commit()
    tweet_media = await session.execute(
        select(TweetsMedia).filter(
            TweetsMedia.user_id == user.id,
            TweetsMedia.media_path == save_path,
            TweetsMedia.media_id == media_id,
            )
    )
    tweet_media = tweet_media.scalars().first()
    return tweet_media.id


async def delete_tweet(tweet_id: int, user: User):
    tweet = await session.execute(
        select(Tweets)
        .options(joinedload(Tweets.media))
        .filter(Tweets.user_id == user.id, Tweets.id == tweet_id)
    )
    tweet = tweet.scalars().first()
    path = [media.media_path for media in tweet.media]
    try:
        os.remove(path[0])
    except IndexError:
        pass
    await session.delete(tweet)
    await session.commit()

    return True


async def delete_like(tweet_id: int, user: User):
    like = await session.execute(
        select(Like).filter(Like.user_id == user.id, Like.tweet_id == tweet_id)
    )
    like = like.scalars().first()

    await session.delete(like)
    await session.commit()
    session.expire_all()

    return True


async def unfollow_user(user: User, user_to_follow: User, follower: Followers):
    following = await session.execute(
        select(Following).filter(
            Following.user_id == user_to_follow.id,
            Following.user_following_id == user.id,
            )
    )
    following = following.scalars().first()
    await session.delete(follower)
    await session.delete(following)
    await session.commit()
    session.expire_all()
    return True

